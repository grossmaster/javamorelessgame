import java.util.ArrayList;
import java.util.Random;

public class Model {
    public final int VALUE = new Random().nextInt(100);
    public int start = 0;
    public int finish = 100;
    public int buffer;
    public ArrayList<Integer> attempts = new ArrayList<>();

    public boolean addAttempt(Integer integer) {
        if (attempts.contains(integer)) {
            return false;
        }
        attempts.add(integer);
        return true;

    }


    public boolean checkBoundsOfRange(int number) {
        return number < start || number > finish;
    }


    public void changeRange(int number) {
        if (number > VALUE) {
            finish = number;
        } else {
            start = number;
        }
    }

    public boolean checkNumberWithValue(int number) {
        return number == VALUE;
    }


}
