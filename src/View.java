import java.util.ArrayList;

public class View {
    public static final String INPUT_INT_DATA = "Input your guess number out of current range: ";
    public static final String WRONG_INPUT_INT_DATA = "Wrong input! Try again please! ";
    public static final String INPUT_INT_DATA_OUT_OF_RANGE = "Input out of range! Try again please! ";
    public static final String ALREADY_TRIED_ATTEMPT = "You have already tried this number! Try something else!";
    public static final String WIN = "Yes! You win! Secret number was: ";
    public static final String STATISTIC = "Your statistic: ";
    public static final String NUMBER_OF_ATTEMPTS = "Amount of attempts: ";


    public void printMessage(String message) {
        System.out.println(message);
    }

    public void printWinMessage(String message, int number) {
        System.out.println(message + number);
    }


    public void printRange(int start, int finish) {
        System.out.println("Current range of numbers is: " + start + " - " + finish);
    }

    public void printPreviousAttempts(ArrayList<Integer> arrayList) {
        System.out.println("Your previous attempts are: ");
        for (Integer integer : arrayList) {
            System.out.print(integer + " ");
        }
        System.out.print("\n");
    }
}
