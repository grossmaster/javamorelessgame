import java.util.Scanner;

public class Controller {
    private final Model model;
    private final View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public int inputIntValueWithScanner(Scanner sc) {
        view.printRange(model.start, model.finish);
        view.printMessage(View.INPUT_INT_DATA);
        while (!sc.hasNextInt()) {
            view.printMessage(View.WRONG_INPUT_INT_DATA + View.INPUT_INT_DATA);
            sc.next();
        }
        model.buffer = sc.nextInt();
        return model.buffer;
    }

    public int inputCheckOutOfRange(Scanner sc) {
        while (model.checkBoundsOfRange(inputIntValueWithScanner(sc))) {
            view.printMessage(View.INPUT_INT_DATA_OUT_OF_RANGE);
        }
        return model.buffer;
    }

    public int inputCheckOnRepeat(Scanner sc) {
        while (!model.addAttempt(inputCheckOutOfRange(sc))) {
            view.printMessage(View.ALREADY_TRIED_ATTEMPT);
        }
        return model.buffer;
    }

    public void processUser() {
        Scanner sc = new Scanner(System.in);

        while (!model.checkNumberWithValue(inputCheckOnRepeat(sc))) {
            model.changeRange(model.buffer);
            view.printPreviousAttempts(model.attempts);
        }
        view.printWinMessage(View.WIN, model.VALUE);
        view.printMessage(View.STATISTIC);
        view.printWinMessage(View.NUMBER_OF_ATTEMPTS, model.attempts.size());
        view.printPreviousAttempts(model.attempts);
    }

}
